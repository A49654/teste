package br.com.afi.poc.build;

import br.com.afi.poc.build.dep.TesteDep;

public class Main {

	public static void main(String[] args) {
		final TesteDep dep = new TesteDep();
		dep.execute();
	}
}
